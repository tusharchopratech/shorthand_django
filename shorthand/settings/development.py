from apps.general import Constants
from base import *

DEBUG = True

Constants.DICTATIONS_PATH = "../dictations/request_dictations/"
Constants.DB_NAME = "shorthand_dev"
Constants.DB_PASSWORD = "tushar"
Constants.DB_USER = "tushar"
Constants.JWT_TOKEN_ALGO = "HS512"
Constants.JWT_TOKEN_REFRESH_KEY = "1234567890"
Constants.JWT_TOKEN_VERIFY = True
Constants.IS_LIVE_PRODUCTION = False
Constants.JWT_TOKEN_KEY = "QWERTYUIO"
LOGS_FOLDER = "../logs/development/"

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': Constants.getDbName(),
        'USER': Constants.getDbUser(),
        'PASSWORD': Constants.getDbPassword(),
        'HOST': '',
        'PORT': '',
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': "%(levelname)s\t[%(asctime)s]\t%(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {

        'errorlog': {
            'level': 'ERROR',
            'class': 'logging.handlers.RotatingFileHandler',
            # 'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': LOGS_FOLDER + "errorlog.log",
            'maxBytes': 50 * 1024 * 1024,
            # 'when': 'midnight',
            'backupCount': 10,
            'formatter': 'standard',
        },
        'warnlog': {
            'level': 'WARN',
            'class': 'logging.handlers.RotatingFileHandler',
            # 'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': LOGS_FOLDER + "warnlog.log",
            'maxBytes': 50 * 1024 * 1024,
            # 'when': 'midnight',
            'backupCount': 10,
            'formatter': 'standard',
        },
        'criticallog': {
            'level': 'CRITICAL',
            'class': 'logging.handlers.RotatingFileHandler',
            # 'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': LOGS_FOLDER + "criticallog.log",
            'maxBytes': 50 * 1024 * 1024,
            # 'when': 'midnight',
            'backupCount': 10,
            'formatter': 'standard',
        },
    },
    'loggers': {
        'errorlog': {
            'handlers': ['errorlog'],
        },
        'warnlog': {
            'handlers': ['warnlog'],
        },
        'criticallog': {
            'handlers': ['criticallog'],
        },
    }
}
