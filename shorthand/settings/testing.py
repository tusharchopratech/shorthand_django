from base import *
from datetime import datetime

DEBUG = True

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {
    'default': {
        #'ENGINE': 'django.db.backends.sqlite3',
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'testingdatabase',
        'USER': 'tushar',
        'PASSWORD': 'tushar',
        'HOST': '',
        'PORT': '',
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': "%(levelname)s\t[%(asctime)s]\t%(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {

        'errorlog': {
            'level': 'ERROR',
            'class': 'logging.handlers.RotatingFileHandler',
            # 'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': "logs/testing/errorlog.log",
            'maxBytes': 50 * 1024 * 1024,
            # 'when': 'midnight',
            'backupCount': 10,
            'formatter': 'standard',
        },
        'warnlog': {
            'level': 'WARN',
            'class': 'logging.handlers.RotatingFileHandler',
            #'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': "logs/testing/warnlog.log",
            'maxBytes': 50 * 1024 * 1024,
            #'when': 'midnight',
            'backupCount': 10,
            'formatter': 'standard',
        },
    },
    'loggers': {
        'errorlog': {
            'handlers': ['errorlog'],
        },
        'warnlog': {
            'handlers': ['warnlog'],
        },
    }
}
