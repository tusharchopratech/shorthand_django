import json
from base import *
from apps.general import Constants

DEBUG = False

mainConfigFilePath = "../shorthand_main_config.json"
with open(mainConfigFilePath) as data_file:
    jsonObject = json.load(data_file)
    Constants.DICTATIONS_PATH = jsonObject["dictation_path"]
    Constants.DB_NAME = jsonObject["db_name"]
    Constants.DB_PASSWORD = jsonObject["db_password"]
    Constants.DB_USER = jsonObject["db_user"]
    Constants.JWT_TOKEN_ALGO = jsonObject["jwt_token_algo"]
    Constants.JWT_TOKEN_REFRESH_KEY = jsonObject["jwt_refresh_token_key"]
    Constants.JWT_TOKEN_VERIFY = jsonObject["jwt_token_verify"]
    Constants.JWT_TOKEN_KEY = jsonObject["jwt_token_key"]
    Constants.IS_LIVE_PRODUCTION = True
    LOGS_FOLDER = jsonObject["logs_path"]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': Constants.getDbName(),
        'USER': Constants.getDbUser(),
        'PASSWORD': Constants.getDbPassword(),
        'HOST': 'localhost',
        'PORT': '',
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': "%(levelname)s\t[%(asctime)s]\t%(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {

        'errorlog': {
            'level': 'ERROR',
            'class': 'logging.handlers.RotatingFileHandler',
            # 'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': LOGS_FOLDER + "errorlog.log",
            'maxBytes': 50 * 1024 * 1024,
            # 'when': 'midnight',
            'backupCount': 10,
            'formatter': 'standard',
        },
        'warnlog': {
            'level': 'WARN',
            'class': 'logging.handlers.RotatingFileHandler',
            # 'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': LOGS_FOLDER + "warnlog.log",
            'maxBytes': 50 * 1024 * 1024,
            # 'when': 'midnight',
            'backupCount': 10,
            'formatter': 'standard',
        },
        'criticallog': {
            'level': 'CRITICAL',
            'class': 'logging.handlers.RotatingFileHandler',
            # 'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': LOGS_FOLDER + "criticallog.log",
            'maxBytes': 50 * 1024 * 1024,
            # 'when': 'midnight',
            'backupCount': 10,
            'formatter': 'standard',
        },
    },
    'loggers': {
        'errorlog': {
            'handlers': ['errorlog'],
        },
        'warnlog': {
            'handlers': ['warnlog'],
        },
    }
}
