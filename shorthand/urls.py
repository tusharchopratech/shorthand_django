"""shorthand URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url

import apps.home.views as home_view
from apps.dictations.views import view_get_audio, view_get_dictation_list, view_get_dictation_types, view_get_image, \
    view_get_offline_file, view_get_offline_file_data
from apps.users.views import view_add_user, view_get_user_details, view_set_bookmark, view_set_feedback, \
    view_set_practiced, view_set_user_dictation_rating, view_set_user_details, view_set_user_metadata, \
    view_get_initial_app_info, view_get_refresh_token, view_analytics, view_set_reset_progress, view_set_viewed


urlpatterns = [

    # url(r'^admin/', admin.site.urls),
    url(r'^$', home_view.index, name="index"),

    url(r'^api/v1/users/v1/add_user', view_add_user.AddUser.as_view()),
    url(r'^api/v1/users/v1/get_user_details', view_get_user_details.GetUserDetails.as_view()),
    url(r'^api/v1/users/v1/get_initial_info', view_get_initial_app_info.GetInitialAppInfo.as_view()),
    url(r'^api/v1/users/v1/get_new_access_token', view_get_refresh_token.GetRefreshToken.as_view()),
    url(r'^api/v1/users/v1/set_user_details', view_set_user_details.SetUserDetails.as_view()),
    url(r'^api/v1/users/v1/set_bookmark', view_set_bookmark.SetBookmark.as_view()),
    url(r'^api/v1/users/v1/set_practiced', view_set_practiced.SetPracticed.as_view()),
    url(r'^api/v1/users/v1/set_viewed', view_set_viewed.SetViewed.as_view()),
    url(r'^api/v1/users/v1/set_feedback', view_set_feedback.SetFeedback.as_view()),
    url(r'^api/v1/users/v1/set_user_dictation_rating', view_set_user_dictation_rating.SetUserDictationRating.as_view()),
    url(r'^api/v1/users/v1/set_user_meta', view_set_user_metadata.SetUserMetaData.as_view()),
    url(r'^api/v1/users/v1/set_reset_progress', view_set_reset_progress.SetResetProgress.as_view()),

    url(r'^api/v1/shorthand_analytics', view_analytics.Analytics.as_view()),

    url(r'^api/v1/dictation/v1/get_dictation_types', view_get_dictation_types.GetDictationTypes.as_view()),
    url(r'^api/v1/dictation/v1/get_dictation_list', view_get_dictation_list.GetDictationList.as_view()),
    url(r'^api/v1/dictation/v1/get_audio', view_get_audio.GetAudio.as_view()),
    url(r'^api/v1/dictation/v1/get_image', view_get_image.GetImage.as_view()),

    url(r'^api/v1/dictation/v1/get_offline_file_data', view_get_offline_file_data.GetOfflineFileData.as_view()),
    url(r'^api/v1/dictation/v1/get_offline_file_2', view_get_offline_file.GetOfflineFile.as_view()),
]
