import uuid

from django.db import models


# Create your models here.

class UserData(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False, null=False)
    name = models.CharField(max_length=100, null=False)
    pic_url = models.CharField(max_length=1000, null=True)
    dob = models.DateField(max_length=10, null=True)
    gender = models.CharField(max_length=6, null=True)
    email = models.CharField(unique=True, max_length=100, null=False)
    phone_no = models.CharField(max_length=20, null=True)
    active_platforms = models.CharField(max_length=100, null=True)
    bookmark = models.CharField(max_length=10000, null=True)
    practiced = models.CharField(max_length=1000000, null=True)
    viewed = models.CharField(max_length=100000, null=True)
    activation_time = models.DateTimeField(auto_now_add=True, editable=False, null=False)
    deactivation_time = models.DateTimeField(null=True)
    last_used = models.DateTimeField(auto_now_add=True, null=False)
    user_type = models.CharField(max_length=100, null=False)
    is_active = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=False)

    class Meta:
        db_table = "user_data"


class UserMeta(models.Model):
    user_uuid = models.UUIDField(null=True)
    platform = models.CharField(max_length=10, null=True)
    platform_version = models.CharField(max_length=10, null=True)
    login_type = models.CharField(max_length=100, null=False)
    login_unique_id = models.CharField(max_length=1000, null=False)
    referrer = models.CharField(max_length=1000, null=True)
    location = models.CharField(max_length=1000, null=True)
    location_accuracy = models.CharField(max_length=1000, null=True)
    location_city = models.CharField(max_length=1000, null=True)
    location_country = models.CharField(max_length=1000, null=True)
    last_login_at = models.DateTimeField(auto_now_add=True, null=False)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=False)

    class Meta:
        unique_together = ('user_uuid', 'platform')
        db_table = "user_meta"


class UsersDictationRating(models.Model):
    user_uuid = models.UUIDField(null=True)
    dictation_uuid = models.UUIDField(null=True)
    passage_difficulty = models.IntegerField(null=False)
    voice_clarity = models.IntegerField(null=False)
    modulation = models.IntegerField(null=False)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=False)

    class Meta:
        unique_together = ('user_uuid', 'dictation_uuid')
        db_table = "user_dictation_rating"

class UsersDictationComment(models.Model):
    user_uuid = models.UUIDField(null=True)
    dictation_uuid = models.UUIDField(null=True)
    comment = models.TextField(null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=False)

    class Meta:
        db_table = "user_dictation_comment"

class UsersFeedback(models.Model):
    user_uuid = models.UUIDField(null=True)
    message = models.TextField(null=True)
    type = models.CharField(max_length=1000, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=False)

    class Meta:
        db_table = "user_feedback"


class ShorthandAnalytics(models.Model):
    user_uuid = models.UUIDField(null=False)
    category = models.CharField(max_length=10000, null=True)
    action = models.CharField(max_length=10000, null=True)
    label = models.CharField(max_length=10000, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=False)

    class Meta:
        db_table = "shorthand_analytics"
