import inspect

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.dictations.models import DictationMeta
from apps.general import Auth
from apps.general import MyLogger
from apps.general import UserLastUsed
from apps.users.models import UsersDictationRating, UsersDictationComment


class SetUserDictationRating(APIView):
    def post(self, request):

        try:
            obj = Auth.TokenUtils().authenticateUserFromRequest(request)
            if isinstance(obj, Response):
                return obj
            else:
                uuid = obj

                data = request.data
                passage_difficulty = data.get('passage_difficulty')
                voice_clarity = data.get('voice_clarity')
                modulation = data.get('modulation')
                dictation_uuid = data.get('dictation_id')
                message = data.get('message')

                if passage_difficulty is None or voice_clarity is None or modulation is None or dictation_uuid is None \
                        or passage_difficulty == "" or voice_clarity == "" or modulation == "" or dictation_uuid == "" \
                        or not (int(passage_difficulty) >= 0 and int(passage_difficulty) <= 5) \
                        or not (int(voice_clarity) >= 0 and int(voice_clarity) <= 5) \
                        or not (int(modulation) >= 0 and int(modulation) <= 5):
                    MyLogger.warning(request, "400", __name__, str(inspect.currentframe().f_lineno))
                    return Response(status=status.HTTP_400_BAD_REQUEST)
                else:

                    try:
                        userDictationComment = UsersDictationComment(user_uuid=uuid, dictation_uuid=dictation_uuid,
                                                                     comment=message)
                        userDictationComment.save()
                    except Exception as e:
                        MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
                        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

                    try:
                        userDictationRating = UsersDictationRating.objects.get(user_uuid=uuid,
                                                                               dictation_uuid=dictation_uuid)
                        userDictationRating.modulation = modulation
                        userDictationRating.voice_clarity = voice_clarity
                        userDictationRating.passage_difficulty = passage_difficulty
                        userDictationRating.save()

                    except UsersDictationRating.DoesNotExist:
                        userDictationRating = UsersDictationRating(user_uuid=uuid, dictation_uuid=dictation_uuid,
                                                                   passage_difficulty=passage_difficulty,
                                                                   modulation=modulation, voice_clarity=voice_clarity)
                        userDictationRating.save()
                    except Exception as e:
                        MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
                        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

                    try:
                        dictationMeta = DictationMeta.objects.get(dictation_uuid=dictation_uuid)
                        dictationMeta.passage_difficulty = ((
                                                            dictationMeta.passage_difficulty * dictationMeta.passage_difficulty_total) + float(
                            passage_difficulty)) / (dictationMeta.passage_difficulty_total + 1)
                        dictationMeta.modulation = ((dictationMeta.modulation * dictationMeta.modulation_total) + float(
                            modulation)) / (dictationMeta.modulation_total + 1)
                        dictationMeta.voice_clarity = ((
                                                       dictationMeta.voice_clarity * dictationMeta.voice_clarity_total) + float(
                            voice_clarity)) / (dictationMeta.voice_clarity_total + 1)

                        dictationMeta.passage_difficulty_total += 1
                        dictationMeta.voice_clarity_total += 1
                        dictationMeta.modulation_total += 1
                        dictationMeta.save()
                    except Exception as e:
                        MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
                        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

                    UserLastUsed.UserLastUsed().setUserLastUsed(uuid)
                    return Response(status=status.HTTP_200_OK)

        except Exception as e:
            MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
