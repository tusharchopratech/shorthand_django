from apps.general import UserLastUsed
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.general import Auth
from apps.users.models import UserMeta

from apps.general import MyLogger
import inspect

class SetUserMetaData(APIView):
    def post(self, request):
        try:
            obj = Auth.TokenUtils().authenticateUserFromRequest(request)
            if isinstance(obj, Response):
                return obj
            else:
                uuid = obj

            data = request.data
            location = data.get('location')
            platform = data.get('platform')
            platform_version = data.get('platform_version')
            referrer = data.get('referrer')

            if location is None or platform is None or platform_version is None or referrer is None:
                MyLogger.warning(request, "400", __name__,str(inspect.currentframe().f_lineno))
                return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                userMeta = None
                try:
                    userMeta = UserMeta.objects.get(user_uuid=uuid)
                except UserMeta.DoesNotExist:
                    userMeta = UserMeta()
                except Exception as e:
                    MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
                    return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

                if location != "":
                    userMeta.location = location
                if platform != "":
                    userMeta.platform = platform
                if platform_version != "":
                    userMeta.platform_version = platform_version
                if referrer != "":
                    userMeta.referrer = referrer
                userMeta.user_uuid = uuid
                userMeta.save()

                UserLastUsed.UserLastUsed().setUserLastUsed(uuid)
                return Response(status=status.HTTP_200_OK)

        except Exception as e:
            MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
