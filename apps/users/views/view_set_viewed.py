import inspect

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.general import Auth
from apps.general import MyLogger
from apps.general import UserLastUsed
from apps.users.models import UserData


class SetViewed(APIView):
    def post(self, request):
        try:
            obj = Auth.TokenUtils().authenticateUserFromRequest(request)
            if isinstance(obj, Response):
                return obj
            else:
                uuid = obj

            data = request.data
            dictation_id = data.get('dictation_id')

            if dictation_id is None or dictation_id == "":
                MyLogger.warning(request, "400", __name__, str(inspect.currentframe().f_lineno))
                return Response(status=status.HTTP_400_BAD_REQUEST)
            else:

                try:
                    userData = UserData.objects.get(uuid=uuid)

                    practicedList = []
                    if userData.practiced is not None and userData.practiced != "":
                        practicedList = userData.practiced.split(",")

                    viewedList = []
                    if userData.viewed is not None and userData.viewed != "":
                        viewedList = userData.viewed.split(",")

                    if dictation_id not in practicedList:
                        if dictation_id not in viewedList:
                            viewedList.append(str(dictation_id))
                            userData.viewed = ",".join(viewedList)
                            userData.save()

                except Exception as e:
                    MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
                    return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

                UserLastUsed.UserLastUsed().setUserLastUsed(uuid)
                return Response(status=status.HTTP_200_OK)

        except Exception as e:
            MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
