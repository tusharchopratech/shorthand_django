import inspect

from django.db.utils import IntegrityError
from django.utils import timezone
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.general import Auth
from apps.general import MyLogger
from apps.general import UserLastUsed
from apps.users.models import UserData, UserMeta

class AddUser(APIView):
    def post(self, request):
        try:
            data = request.data
            login_type = data.get('login_type').lower()
            login_unique_id = data.get('login_unique_id')
            email = data.get('email')
            name = data.get('name').lower()
            pic_url = data.get('pic_url')
            platform = data.get('platform')
            platform_version = data.get('platform_version')
            location = data.get('location')
            accuracy = data.get('location_accuracy')
            location_city = data.get('location_city')
            location_country = data.get('location_country')
            referrer = data.get('referrer')

            if login_unique_id is None or login_type is None or email is None or name is None or pic_url is None or platform is None or accuracy is None \
                    or platform_version is None or location is None or location_city is None or location_country is None or referrer is None \
                    or login_unique_id == "" or login_type == "" or email == "" or name == "" or platform == "" \
                    or platform_version == "":
                MyLogger.warning(request, "400", __name__, str(inspect.currentframe().f_lineno))
                return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                try:
                    userType = "new"
                    user = UserData(name=name, email=email, last_used=timezone.now(), user_type="free", pic_url=pic_url,
                                    active_platforms=platform)
                    user.save()
                except IntegrityError as e:
                    print str(e)
                    userType = "old"
                    user = UserData.objects.get(email=email)
                    if platform not in user.active_platforms:
                        user.active_platforms = user.active_platforms + "," + platform

                try:
                    user_meta = UserMeta.objects.get(user_uuid=user.uuid, platform=platform)
                    user_meta.login_type = login_type
                    user_meta.login_unique_id = login_unique_id
                    user_meta.platform_version = platform_version
                    user_meta.referrer = referrer
                    user_meta.location = location
                    user_meta.location_accuracy = accuracy
                    user_meta.location_city = location_city
                    user_meta.location_country = location_country
                    user_meta.last_login_at = timezone.now()
                    user_meta.save()
                except UserMeta.DoesNotExist:
                    user_meta = UserMeta(user_uuid=user.uuid, login_type=login_type, login_unique_id=login_unique_id,
                                         platform=platform, platform_version=platform_version, referrer=referrer,
                                         location=location, location_city=location_city, location_accuracy=accuracy,
                                         location_country=location_country, last_login_at=timezone.now())
                    user_meta.save()

                auth = Auth.TokenUtils()
                token = auth.getFiniteToken(str(user.uuid), email)
                refreshToken = auth.getInfiniteRefreshToken(str(user.uuid), email, name)
                uuidString = str(user.uuid)
                response = {
                    'token': token,
                    'refresh_token': refreshToken,
                    'uuid': uuidString,
                    'type': userType
                }

                UserLastUsed.UserLastUsed().setUserLastUsed(uuidString)
                return Response(response, status=status.HTTP_200_OK)

        except Exception as e:
            MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
