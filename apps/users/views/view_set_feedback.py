import datetime
import inspect

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.general import Auth
from apps.general import MyLogger
from apps.general import UserLastUsed
from apps.users.models import UsersFeedback

class SetFeedback(APIView):
    def post(self, request):
        try:
            obj = Auth.TokenUtils().authenticateUserFromRequest(request)
            if isinstance(obj, Response):
                return obj
            else:
                uuid = obj

            data = request.data
            feedback = data.get('feedback')
            type = data.get('type')

            if feedback is None or feedback == "" or type is None or type == "":
                MyLogger.warning(request, "400", __name__, str(inspect.currentframe().f_lineno))
                return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                userFeedback = UsersFeedback(user_uuid=uuid, message=feedback, type=type,
                                             created_at=datetime.datetime.now())
                userFeedback.save()

                UserLastUsed.UserLastUsed().setUserLastUsed(uuid)
                return Response(status=status.HTTP_200_OK)

        except Exception as e:
            MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
