import inspect

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.general import Auth
from apps.general import MyLogger
from apps.general import UserLastUsed
from apps.users.models import UserData


class SetUserDetails(APIView):
    def post(self, request):
        try:
            obj = Auth.TokenUtils().authenticateUserFromRequest(request)
            if isinstance(obj, Response):
                return obj
            else:
                uuid = obj

            dob = request.data.get('dob')
            name = request.data.get('name').lower()
            gender = request.data.get('gender').lower()
            phoneNo = request.data.get('phone_no')

            if dob is None or dob == "" or gender is None or gender == "" or not (
                                gender == "male" or gender == "female" or gender == "other") or phoneNo is None or phoneNo == "":
                MyLogger.warning(request, "400", __name__, str(inspect.currentframe().f_lineno))
                return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                try:
                    userData = UserData.objects.get(uuid=uuid)
                    userData.phone_no = phoneNo
                    userData.dob = dob
                    userData.gender = gender
                    userData.name = name
                    userData.save()
                except Exception as e:
                    MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
                    return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            UserLastUsed.UserLastUsed().setUserLastUsed(uuid)
            return Response(status=status.HTTP_200_OK)

        except Exception as e:
            MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
