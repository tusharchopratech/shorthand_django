import inspect

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.general import Auth
from apps.general import MyLogger
from apps.general import UserLastUsed
from apps.users.models import UserData, UsersDictationRating


class GetUserDetails(APIView):
    def post(self, request):
        try:
            obj = Auth.TokenUtils().authenticateUserFromRequest(request)
            if isinstance(obj, Response):
                return obj
            else:
                uuid = obj
                user = UserData.objects.get(uuid=uuid)
                bookmarksList = []
                practicedList = []
                rating = []

                if user.bookmark is not None:
                    bookmarksList = user.bookmark.split(",")
                if user.practiced is not None:
                    practicedList = user.practiced.split(",")
                try:
                    userDictationRating = UsersDictationRating.objects.filter(user_uuid=uuid)
                    for object in userDictationRating:
                        if object.dictation_uuid is not None:
                            subObject = {
                                'dictation_id': str(object.dictation_uuid),
                                'passage_difficulty': object.passage_difficulty,
                                'voice_clarity': object.voice_clarity,
                                'modulation': object.modulation
                            }
                            rating.append(subObject)
                except UsersDictationRating.DoesNotExist:
                    pass
                except Exception as e:
                    MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
                    return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

                response = {
                    'name': user.name.lower(),
                    'dob': user.dob,
                    'gender': user.gender.lower(),
                    'phone_no': user.phone_no,
                    'user_type': user.user_type.lower(),
                    'bookmarks': bookmarksList,
                    'practiced': practicedList,
                    'rating': rating
                }

                UserLastUsed.UserLastUsed().setUserLastUsed(uuid)
                return Response(response, status=status.HTTP_200_OK)

        except Exception as e:
            MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
