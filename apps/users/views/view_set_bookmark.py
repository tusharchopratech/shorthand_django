import inspect

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.general import Auth
from apps.general import MyLogger
from apps.general import UserLastUsed
from apps.users.models import UserData


class SetBookmark(APIView):
    def post(self, request):

        try:
            obj = Auth.TokenUtils().authenticateUserFromRequest(request)
            if isinstance(obj, Response):
                return obj
            else:
                uuid = obj

            data = request.data
            bookmark = data.get('bookmark')
            set = data.get('set')

            if bookmark is None or bookmark == "" or set is None or set == "" or not (set == "1" or set == "0"):
                MyLogger.warning(request, "400", __name__, str(inspect.currentframe().f_lineno))
                return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                if set == "1":
                    try:
                        userData = UserData.objects.get(uuid=uuid)

                        if userData.bookmark is None:
                            userData.bookmark = bookmark
                            userData.save()
                        elif bookmark not in userData.bookmark:
                            bookmarkList = userData.bookmark.split(",")
                            bookmarkList.extend(str(bookmark))
                            userData.bookmark = ",".join(bookmarkList)
                            userData.save()

                    except Exception as e:
                        MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
                        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

                elif set == "0":
                    try:
                        userData = UserData.objects.get(uuid=uuid)
                        if userData.bookmark is not None:
                            bookmarkList = userData.bookmark.split(",")
                            bookmarkList.remove(str(bookmark))
                            if len(bookmarkList) == 0:
                                userData.bookmark = None
                            else:
                                userData.bookmark = ",".join(bookmarkList)
                            userData.save()
                    except Exception as e:
                        MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
                        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

                UserLastUsed.UserLastUsed().setUserLastUsed(uuid)
                return Response(status=status.HTTP_200_OK)

        except Exception as e:
            MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
