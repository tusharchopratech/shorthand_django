import inspect

from django.db import connection
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.general import Auth
from apps.general import Constants
from apps.general import MyLogger
from apps.general import UserLastUsed


class GetInitialAppInfo(APIView):
    def post(self, request):
        try:
            obj = Auth.TokenUtils().authenticateUserFromRequest(request)
            if isinstance(obj, Response):
                return obj
            else:
                uuid = obj

            app_version = int(request.data.get('app_version'))
            platform = request.data.get('platform')

            if app_version is None or app_version == "" or platform is None or platform == "":
                MyLogger.warning(request, "400", __name__, str(inspect.currentframe().f_lineno))
                return Response(status=status.HTTP_400_BAD_REQUEST)

            if app_version < int(Constants.getAndroidMinimumVersion()):
                return Response({'update_app': 'yes', 'dictation_types': []}, status=status.HTTP_200_OK)
            else:
                cursor = connection.cursor()
                cursor.execute('select sum(times_played), speed, duration, language,'
                               ' count(*) from dictation group by speed, duration, language;')
                rows = cursor.fetchall()

                data = []
                for row in rows:
                    data.append({'speed': row[1], 'duration': row[2], 'times_played': row[0], 'language': row[3],
                                 'number_of_recordings': row[4]})

                update_app = 'no'
                if int(Constants.getAndroidMinimumVersion()) <= app_version < int(Constants.getAndroidCurrentVersion()):
                    update_app = 'optional'

                response = {
                    'update_app': update_app,
                    'dictation_types': data,
                    'ads_banner_percent': Constants.getAdBannerPercentage(),
                    'ads_interstitial_dictation_list_percent': Constants.getAdInterstitialDictationListPercentage(),
                    'ads_interstitial_dictation_type_percent': Constants.getAdInterstitialDictationTypePercentage(),
                    'ads_rewarded_video_percent': Constants.getAdRewardedVideoPercentage(),
                    'receive_analytics': Constants.getReceiveAnalytics()
                }

                UserLastUsed.UserLastUsed().setUserLastUsed(uuid)
                return Response(response, status=status.HTTP_200_OK)

        except Exception as e:
            MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
