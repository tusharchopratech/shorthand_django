import inspect

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.general import Auth
from apps.general import MyLogger
from apps.general import UserLastUsed
from apps.users.models import ShorthandAnalytics

class Analytics(APIView):
    def post(self, request):
        try:
            obj = Auth.TokenUtils().authenticateUserFromRequest(request)
            if isinstance(obj, Response):
                return obj
            else:
                uuid = obj

            category = request.data.get('category')
            action = request.data.get('action')
            label = request.data.get('label')

            if category is None or category == "" or action is None or action == "" or label is None or label == "":
                MyLogger.warning(request, "400", __name__, str(inspect.currentframe().f_lineno))
                return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                try:
                    shorthand_analytics = ShorthandAnalytics(user_uuid=uuid, category=category, action=action,
                                                             label=label)
                    shorthand_analytics.save()
                except Exception as e:
                    MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
                    return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            UserLastUsed.UserLastUsed().setUserLastUsed(uuid)
            return Response(status=status.HTTP_200_OK)

        except Exception as e:
            MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
