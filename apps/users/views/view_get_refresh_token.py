import inspect

import jwt
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.general import Auth
from apps.general import Constants
from apps.general import MyLogger
from apps.general import UserLastUsed

class GetRefreshToken(APIView):
    def post(self, request):
        try:
            refresh_token = request.META.get('HTTP_AUTHORIZATION', None)
            clientSHA = request.META.get('HTTP_KEY', None)

            payload = jwt.decode(refresh_token, Constants.getJwtRefreshTokenKey(), verify=False)
            uuid = payload.get('uuid')
            email = payload.get('email')
            name = payload.get('name').lower()

            auth = Auth.TokenUtils()

            serverSHA = auth.getShaFromUuidEmailName(uuid, email, name)

            if clientSHA == serverSHA:
                token = auth.getFiniteToken(uuid, email)
                response = {
                    'token': token,
                }
                UserLastUsed.UserLastUsed().setUserLastUsed(uuid)
                return Response(response, status=status.HTTP_200_OK)
            else:
                MyLogger.warning(request, "403", __name__, str(inspect.currentframe().f_lineno))
                print "Error : Sha wrong [ CLIENT SHA : " + clientSHA + " ] [ SERVER SHA : " + serverSHA + " ]"
                return Response(status=status.HTTP_403_FORBIDDEN)

        except jwt.ExpiredSignature:
            MyLogger.warning(request, "401", __name__, str(inspect.currentframe().f_lineno))
            return Response({'error': "Token is expired"}, status=status.HTTP_401_UNAUTHORIZED)
        except jwt.DecodeError:
            MyLogger.warning(request, "403", __name__, str(inspect.currentframe().f_lineno))
            print "Error : Cannot decode token"
            return Response({'error': "Cannot decode token"}, status=status.HTTP_403_FORBIDDEN)
        except jwt.InvalidTokenError:
            MyLogger.warning(request, "403", __name__, str(inspect.currentframe().f_lineno))
            print "Error : Token is invalid"
            return Response({'error': "Token is invalid"}, status=status.HTTP_403_FORBIDDEN)
        except Exception as e:
            MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
