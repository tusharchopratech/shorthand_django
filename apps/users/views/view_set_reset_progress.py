import inspect

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.general import Auth
from apps.general import MyLogger
from apps.general import UserLastUsed
from apps.users.models import UserData

class SetResetProgress(APIView):
    def post(self, request):
        try:
            obj = Auth.TokenUtils().authenticateUserFromRequest(request)
            if isinstance(obj, Response):
                return obj
            else:
                uuid = obj

                try:
                    userData = UserData.objects.get(uuid=uuid)
                    userData.viewed = None
                    userData.practiced = None
                    userData.save()

                except Exception as e:
                    MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
                    return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

                UserLastUsed.UserLastUsed().setUserLastUsed(uuid)
                return Response(status=status.HTTP_200_OK)

        except Exception as e:
            MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
