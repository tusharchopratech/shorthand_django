import hashlib
import json

import psycopg2
from django.test import TransactionTestCase
from rest_framework.test import RequestsClient


class UserTest(TransactionTestCase):
    token = ""
    uuid = ""
    email = "tusharchopra12345@gmail.com"

    def test_user(self):
        try:
            conn = psycopg2.connect("dbname='test_shorthand_dev' user='tushar' host='' password='tushar'")
        except Exception as e:
            assert "Unable to connect database." + str(e)

        print "\n\n****** USER APP TEST ******\n"

        self.checkIfDictationsPresent()
        self.cloningDictationDatabaseForTesting(conn)
        self.checkAddUserApi(conn)
        self.checkSetBookmarkApi(conn)
        self.checkSetPractcedApi(conn)
        self.checkSetUserMetaDataApi(conn)
        self.checkSetUserDetailsApi(conn)
        self.checkSetUserDictationRatingApi(conn)
        self.checkSetFeedbackApi(conn)
        self.checkGetUserDetailsApi(conn)
        conn.close()


    def getSHA(self, uuid, email):
        string = uuid + "volsky" + email
        m = hashlib.sha1()
        m.update(string.encode('utf-8'))
        return m.hexdigest()

    def checkIfDictationsPresent(self):
        try:
            conn = psycopg2.connect("dbname='shorthand_dev' user='tushar' host='' password='tushar'")
        except Exception as e:
            assert "Unable to connect database." + str(e)
        cur = conn.cursor()
        cur.execute("SELECT * from dictation;")
        rows = cur.fetchall()
        assert len(rows) >= 144
        cur.execute("SELECT * from dictation_meta;")
        rows = cur.fetchall()
        assert len(rows) >= 144
        cur.close()
        print "Checking Dictation Present In Main DB : PASSED"

    def cloningDictationDatabaseForTesting(self, conn):
        try:
            connMainDb = psycopg2.connect("dbname='shorthand_dev' user='tushar' host='' password='tushar'")
        except Exception as e:
            assert "Unable to connect database." + str(e)

        connMainDb.autocommit = True
        conn.autocommit = True
        cur = connMainDb.cursor()
        cur.execute("SELECT * from dictation;")
        rows = cur.fetchall()
        connMainDb.close()

        cur = conn.cursor()
        cur.execute('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')
        cur.execute('ALTER TABLE dictation ALTER COLUMN uuid SET DEFAULT uuid_generate_v4();')
        cur.execute('ALTER TABLE dictation ALTER COLUMN created SET DEFAULT NOW();')

        for row in rows:
            insertQuery = "insert into dictation(recording_name, speaker_name, speaker_gender, language, topic, speed, duration, audio_file_name, image_file_name) values(%s,%s,%s,%s,%s,%s,%s,%s,%s) returning uuid;"
            cur.execute(insertQuery, (row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10]))
            uuid = cur.fetchone()[0]
            insertQuery = "insert into dictation_meta(recording_name,dictation_uuid, times_played, tags, passage_difficulty, passage_difficulty_total, voice_clarity, voice_clarity_total, modulation, modulation_total) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"
            cur.execute(insertQuery, (row[2], uuid, int(297) * 4, row[6], 0, 0, 0, 0, 0, 0))
        cur.close()
        print "Cloning Dictation Database : PASSED"



    def checkAddUserApi(self, conn):
        url = 'http://testserver/api/v1/users/v1/add_user'
        data = {
            'login_type': 'google',
            'login_unique_id': '1234567890',
            'name': 'tushar',
            'email': self.email
        }
        client = RequestsClient()
        response = client.post(url, data)
        assert response.status_code == 200
        assert response.json()['uuid'] != ''
        assert response.json()['token'] != ''
        assert response.json()['refresh_token'] != ''
        self.token = response.json()['token']
        self.uuid = response.json()['uuid']
        cur = conn.cursor()
        cur.execute("SELECT uuid,name,email,login_type,login_unique_id from user_data;")
        rows = cur.fetchall()
        cur.close()
        assert len(rows) == 1
        assert rows[0][1] == "tushar"
        assert rows[0][2] == self.email
        assert rows[0][3] == "google"
        assert rows[0][4] == "1234567890"
        assert str(rows[0][0]) == self.uuid
        print "Add User API : PASSED"


    def checkSetBookmarkApi(self, conn):
        url = 'http://testserver/api/v1/users/v1/set_bookmark'
        client = RequestsClient()
        headers = {
            "AUTHORIZATION": self.token,
            "KEY": self.getSHA(self.uuid, self.email)
        }
        data = {
            'bookmark': 'x',
            'set': "1"
        }
        response = client.post(url, data, headers=headers)
        assert response.status_code == 200, conn.close()
        cur = conn.cursor()
        cur.execute("SELECT bookmark from user_meta where user_uuid = '"+self.uuid+"';")
        rows = cur.fetchall()
        cur.close()
        assert rows[0][0] == 'x', conn.close()
        data = {
            'bookmark': 'y',
            'set': "1"
        }
        response = client.post(url, data, headers=headers)
        assert response.status_code == 200, conn.close()
        cur = conn.cursor()
        cur.execute("SELECT bookmark from user_meta where user_uuid = '"+self.uuid+"';")
        rows = cur.fetchall()
        cur.close()
        assert rows[0][0] == 'x,y', conn.close()

        data = {
            'bookmark': 'y',
            'set': "0"
        }
        response = client.post(url, data, headers=headers)
        assert response.status_code == 200, conn.close()
        cur = conn.cursor()
        cur.execute("SELECT bookmark from user_meta where user_uuid = '"+self.uuid+"';")
        rows = cur.fetchall()
        cur.close()
        assert rows[0][0] == 'x', conn.close()
        print "Set Bookmark API : PASSED"


    def checkSetPractcedApi(self, conn):
        url = 'http://testserver/api/v1/users/v1/set_practiced'
        client = RequestsClient()
        headers = {
            "AUTHORIZATION": self.token,
            "KEY": self.getSHA(self.uuid, self.email)
        }
        data = {
            'practiced': 'x',
            'set': "1"
        }

        response = client.post(url, data, headers=headers)
        assert response.status_code == 200, conn.close()
        cur = conn.cursor()
        cur.execute("SELECT practiced from user_meta where user_uuid = '"+self.uuid+"';")
        rows = cur.fetchall()
        cur.close()
        assert rows[0][0] == 'x', conn.close()

        data = {
            'practiced': 'y',
            'set': "1"
        }
        response = client.post(url, data, headers=headers)
        assert response.status_code == 200, conn.close()
        cur = conn.cursor()
        cur.execute("SELECT practiced from user_meta where user_uuid = '"+self.uuid+"';")
        rows = cur.fetchall()
        cur.close()
        assert rows[0][0] == 'x,y', conn.close()

        data = {
            'practiced': 'y',
            'set': "0"
        }
        response = client.post(url, data, headers=headers)
        assert response.status_code == 200, conn.close()
        cur = conn.cursor()
        cur.execute("SELECT practiced from user_meta where user_uuid = '"+self.uuid+"';")
        rows = cur.fetchall()
        cur.close()
        assert rows[0][0] == 'x', conn.close()
        print "Set Practiced API : PASSED"


    def checkSetUserMetaDataApi(self, conn):
        url = 'http://testserver/api/v1/users/v1/set_user_meta'
        client = RequestsClient()
        headers = {
            "AUTHORIZATION": self.token,
            "KEY": self.getSHA(self.uuid, self.email)
        }
        data = {
            'location': 'x',
            'platform': "x",
            'referrer': "x",
            'platform_version': "x"
        }
        response = client.post(url, data, headers=headers)
        assert response.status_code == 200, conn.close()
        cur = conn.cursor()
        cur.execute("SELECT platform, location, referrer, platform_version from user_meta where user_uuid = '"+self.uuid+"';")
        rows = cur.fetchall()
        cur.close()
        assert rows[0][0] == 'x', conn.close()
        assert rows[0][1] == 'x', conn.close()
        assert rows[0][2] == 'x', conn.close()
        assert rows[0][3] == 'x', conn.close()
        print "Set User Meta Data API : PASSED"

    def checkSetUserDetailsApi(self,conn):
        url = 'http://testserver/api/v1/users/v1/set_user_details'
        client = RequestsClient()
        headers = {
            "AUTHORIZATION": self.token,
            "KEY": self.getSHA(self.uuid, self.email)
        }
        data = {
            'dob': '2012-01-01',
            'gender': "male",
            'phone_no': "9555248166",
        }
        response = client.post(url, data, headers=headers)
        assert response.status_code == 200, conn.close()
        cur = conn.cursor()
        cur.execute("SELECT dob, gender, phone_no from user_data where uuid = '"+self.uuid+"';")
        rows = cur.fetchall()
        cur.close()
        assert str(rows[0][0]) == '2012-01-01', conn.close()
        assert rows[0][1] == 'male', conn.close()
        assert rows[0][2] == '9555248166', conn.close()
        print "Set User Details API : PASSED"


    def checkSetFeedbackApi(self, conn):
        url = 'http://testserver/api/v1/users/v1/set_feedback'
        client = RequestsClient()
        headers = {
            "AUTHORIZATION": self.token,
            "KEY": self.getSHA(self.uuid, self.email)
        }
        data = {
            'feedback': 'This is feedback.',
        }
        response = client.post(url, data, headers=headers)
        assert response.status_code == 200, conn.close()
        cur = conn.cursor()
        cur.execute("SELECT message from user_feedback where user_uuid = '"+self.uuid+"';")
        rows = cur.fetchall()
        cur.close()
        assert rows[0][0] == 'This is feedback.', conn.close()
        print "Set Feedback API: PASSED"


    def checkSetUserDictationRatingApi(self, conn):

        cur = conn.cursor()
        cur.execute("SELECT * from dictation;")
        rows = cur.fetchall()
        dictation_uuid = rows[0][1]
        cur.close()

        url = 'http://testserver/api/v1/users/v1/set_user_dictation_rating'
        client = RequestsClient()
        headers = {
            "AUTHORIZATION": self.token,
            "KEY": self.getSHA(self.uuid, self.email)
        }
        data = {
            'passage_difficulty': 3,
            'voice_clarity': 3,
            'modulation': 3,
            'dictation_id': dictation_uuid
        }
        response = client.post(url, data, headers=headers)
        assert response.status_code == 200, conn.close()
        cur = conn.cursor()
        cur.execute("SELECT platform, location, referrer, platform_version from user_meta where user_uuid = '"+self.uuid+"';")
        rows = cur.fetchall()
        cur.close()
        assert rows[0][0] == 'x', conn.close()
        assert rows[0][1] == 'x', conn.close()
        assert rows[0][2] == 'x', conn.close()
        assert rows[0][3] == 'x', conn.close()
        print "Set User Dictation Rating API : PASSED"


    def checkGetUserDetailsApi(self, conn):
        url = 'http://testserver/api/v1/users/v1/get_user_details'
        client = RequestsClient()
        headers = {
            "AUTHORIZATION": self.token,
            "KEY": self.getSHA(self.uuid, self.email)
        }
        response = client.post(url, headers=headers)
        assert response.status_code == 200, conn.close()
        #print (json.dumps(response.json(), indent=2))
        assert response.json()['name'] == 'tushar', conn.close()
        assert len(response.json()['bookmarks']) == 1, conn.close()
        assert response.json()['bookmarks'][0] == 'x', conn.close()
        assert len(response.json()['practiced']) == 1, conn.close()
        assert response.json()['practiced'][0] == 'x', conn.close()
        print "Get User Details API: PASSED"
