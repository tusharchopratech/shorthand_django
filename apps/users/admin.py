from django.contrib import admin

from .models import UserData, UsersDictationRating, UserMeta, UsersFeedback

admin.site.register(UserData)
admin.site.register(UsersDictationRating)
admin.site.register(UsersFeedback)
admin.site.register(UserMeta)
# Register your models here.

