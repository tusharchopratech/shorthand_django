from rest_framework import status
from django.http import HttpResponse
from rest_framework.decorators import api_view


@api_view(['POST', 'GET'])
def ResponseCode_400_BAD_REQUEST(request):
    return HttpResponse(status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST', 'GET'])
def ResponseCode_403_FORBIDDEN(request):
    return HttpResponse(status=status.HTTP_403_FORBIDDEN)


@api_view(['POST', 'GET'])
def ResponseCode_410_GONE(request):
    return HttpResponse(status=status.HTTP_410_GONE)

@api_view(['POST', 'GET'])
def ResponseCode_404_GONE(request):
    return HttpResponse(status=status.HTTP_404_NOT_FOUND)
