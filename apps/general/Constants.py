import json

projectConfigFilePath = "config.json"

DICTATIONS_PATH = ""
DB_NAME = ""
DB_PASSWORD = ""
DB_USER = ""
JWT_TOKEN_KEY = ""
JWT_TOKEN_ALGO = ""
JWT_TOKEN_REFRESH_KEY = ""
JWT_TOKEN_VERIFY = True
IS_LIVE_PRODUCTION = True


def getDictationPath():
    return DICTATIONS_PATH


def getJwtTokenKey():
    return JWT_TOKEN_KEY


def getJwtRefreshTokenKey():
    return JWT_TOKEN_REFRESH_KEY


def getJwtTokenAlgo():
    return JWT_TOKEN_ALGO


def getJwtTokenVerify():
    return JWT_TOKEN_VERIFY

def getDbName():
    return DB_NAME


def getDbUser():
    return DB_USER


def getDbPassword():
    return DB_PASSWORD


def getIsLiveInProduction():
    return IS_LIVE_PRODUCTION

def getOfflineFilePath():
    with open(projectConfigFilePath) as data_file:
        jsonObject = json.load(data_file)
    return jsonObject["offline_dictation_file_path"]


def getOfflineFileUrl():
    with open(projectConfigFilePath) as data_file:
        jsonObject = json.load(data_file)
        fileUrlArray = jsonObject["offline_dictation_file_data"]
    return fileUrlArray[len(fileUrlArray) - 1]["url"]


def getOfflineFileVersion():
    with open(projectConfigFilePath) as data_file:
        jsonObject = json.load(data_file)
        fileUrlArray = jsonObject["offline_dictation_file_data"]
    return fileUrlArray[len(fileUrlArray) - 1]["version"]


def getOfflineFilePassword():
    with open(projectConfigFilePath) as data_file:
        jsonObject = json.load(data_file)
        fileUrlArray = jsonObject["offline_dictation_file_data"]
    return fileUrlArray[len(fileUrlArray) - 1]["password"]


def getOfflineFileIV():
    with open(projectConfigFilePath) as data_file:
        jsonObject = json.load(data_file)
        fileUrlArray = jsonObject["offline_dictation_file_data"]
    return fileUrlArray[len(fileUrlArray) - 1]["iv"]


def getAndroidMinimumVersion():
    with open(projectConfigFilePath) as data_file:
        jsonObject = json.load(data_file)
    return jsonObject["android_min_version"]


def getAndroidCurrentVersion():
    with open(projectConfigFilePath) as data_file:
        jsonObject = json.load(data_file)
    return jsonObject["android_current_version"]


def getAdBannerPercentage():
    with open(projectConfigFilePath) as data_file:
        jsonObject = json.load(data_file)
    return int(jsonObject["android_show_ad_banner_percentage"])


def getAdRewardedVideoPercentage():
    with open(projectConfigFilePath) as data_file:
        jsonObject = json.load(data_file)
    return int(jsonObject["android_show_ad_reward_video_percentage"])


def getAdInterstitialDictationTypePercentage():
    with open(projectConfigFilePath) as data_file:
        jsonObject = json.load(data_file)
    return int(jsonObject["android_show_ad_interstitial_percentage_dictation_type"])


def getAdInterstitialDictationListPercentage():
    with open(projectConfigFilePath) as data_file:
        jsonObject = json.load(data_file)
    return int(jsonObject["android_show_ad_interstitial_percentage_dictation_list"])


def getServerVersion():
    with open(projectConfigFilePath) as data_file:
        jsonObject = json.load(data_file)
    return jsonObject["server_version"]

def getServerType():
    with open(projectConfigFilePath) as data_file:
        jsonObject = json.load(data_file)
    return jsonObject["server_type"]


def getReceiveAnalytics():
    with open(projectConfigFilePath) as data_file:
        jsonObject = json.load(data_file)
    return jsonObject["receive_analytics"]
