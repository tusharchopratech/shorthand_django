import logging

import requests


def error(request, response_code, fileName, lineNumber, errorString):
    text = "[" + fileName + ":" + lineNumber + "]\t" + request.method + "\t" + request.get_full_path() + "\t " + \
           response_code + " \t[ Error : " + errorString + " ] \t" + "[ Ip Address : " + getClientIp(
        request) + " ]\t[ Request Body : " + str(request.data) + "]\t [ Location : " + getLocation(request) + " ]"
    print "Error : " + text
    logging.getLogger("errorlog").error(text)


def warning(request, response_code, fileName, lineNumber):
    authorization = request.META.get('HTTP_AUTHORIZATION', "")
    sha = request.META.get('HTTP_KEY', "")
    text = "[" + fileName + ":" + lineNumber + "]\t" + request.method + "\t" + request.get_full_path() + "\t " + response_code + \
           " \t[ Ip Address : " + getClientIp(request) + " ]\t[ Request Body : " + str(
        request.data) + " ]" + "\t[ Authorization/Token : " \
           + authorization + " ]\t[ Key/SHA : " + sha + " ]\t[ Location  : " + getLocation(request) + " ]"
    print "Warning : " + text
    logging.getLogger("warnlog").warn(text)


def getClientIp(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        # print "returning FORWARDED_FOR"
        ip = x_forwarded_for.split(',')[-1].strip()
    elif request.META.get('HTTP_X_REAL_IP'):
        # print "returning REAL_IP"
        ip = request.META.get('HTTP_X_REAL_IP')
    else:
        # print "returning REMOTE_ADDR"
        ip = request.META.get('REMOTE_ADDR')
    return ip


def getLocation(request):
    url = 'http://testserver/api/v1/users/v1/get_user_details'
    send_url = 'https://ipapi.co/27.6.233.149/json/'
    return ""
