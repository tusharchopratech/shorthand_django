import datetime
import hashlib
import inspect
from datetime import datetime
from datetime import timedelta

import jwt
from rest_framework import status
from rest_framework.response import Response

import Constants
from apps.general import MyLogger


class TokenUtils:
    def getFiniteToken(self, uuid, email):
        payload = {
            'uuid': uuid,
            'email': email,
            'exp': datetime.utcnow() + timedelta(minutes=100)
        }
        return jwt.encode(payload, Constants.getJwtTokenKey(), algorithm=Constants.getJwtTokenAlgo())

    def getInfiniteRefreshToken(self, uuid, email, name):
        payload = {
            'uuid': uuid,
            'email': email,
            'name': name,
        }
        return jwt.encode(payload, Constants.getJwtRefreshTokenKey(), algorithm=Constants.getJwtTokenAlgo())

    def authenticateUserFromRequest(self, request):
        try:
            userToken = request.META.get('HTTP_AUTHORIZATION', None)
            userSHA = request.META.get('HTTP_KEY', None)

            payload = jwt.decode(userToken, Constants.getJwtTokenKey(), verify=Constants.getJwtTokenVerify())
            uuid = payload.get('uuid')
            email = payload.get('email')

            serverSHA = self.getShaFromUuidEmail(uuid=uuid, email=email)

            if serverSHA == userSHA:
                return uuid
            else:
                MyLogger.warning(request, "403", __name__, str(inspect.currentframe().f_lineno))
                print "Error : Sha wrong"
                return Response(status=status.HTTP_403_FORBIDDEN)
        except jwt.ExpiredSignature:
            return Response(status=status.HTTP_403_FORBIDDEN)
        except jwt.DecodeError:
            MyLogger.warning(request, "403", __name__, str(inspect.currentframe().f_lineno))
            print "Error : Cannot decode token"
            return Response(status=status.HTTP_403_FORBIDDEN)
        except jwt.InvalidTokenError:
            MyLogger.warning(request, "403", __name__, str(inspect.currentframe().f_lineno))
            print "Error : Token is invalid"
            return Response(status=status.HTTP_403_FORBIDDEN)
        except Exception as e:
            MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def authenticateUserFromParameters(self, request, userToken, userSHA):
        try:

            payload = jwt.decode(userToken, Constants.getJwtTokenKey(), verify=Constants.getJwtTokenVerify())
            uuid = payload.get('uuid')
            email = payload.get('email')

            serverSHA = self.getShaFromUuidEmail(uuid=uuid, email=email)

            if serverSHA == userSHA:
                return uuid
            else:
                MyLogger.warning(request, "403", __name__, str(inspect.currentframe().f_lineno))
                print "Error : Sha wrong"
                return Response(status=status.HTTP_403_FORBIDDEN)
        except jwt.ExpiredSignature:
            return Response(status=status.HTTP_403_FORBIDDEN)
        except jwt.DecodeError:
            MyLogger.warning(request, "403", __name__, str(inspect.currentframe().f_lineno))
            print "Error : Cannot decode token"
            return Response(status=status.HTTP_403_FORBIDDEN)
        except jwt.InvalidTokenError:
            MyLogger.warning(request, "403", __name__, str(inspect.currentframe().f_lineno))
            print "Error : Token is invalid"
            return Response(status=status.HTTP_403_FORBIDDEN)
        except Exception as e:
            MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def getShaFromUuidEmail(self, uuid, email):
        string = uuid + "volsky" + email
        m = hashlib.sha1()
        m.update(string.encode('utf-8'))
        return m.hexdigest()

    def getShaFromUuidEmailName(self, uuid, email, name):
        string = uuid + "volsky" + email + "robots" + name
        m = hashlib.sha1()
        m.update(string.encode('utf-8'))
        return m.hexdigest()
