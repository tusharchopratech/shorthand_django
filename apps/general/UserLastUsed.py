from django.utils import timezone

from apps.users.models import UserData


class UserLastUsed:
    def setUserLastUsed(self, uuid):
        try:
            user = UserData.objects.get(uuid=uuid)
            user.last_used = timezone.now()
            user.save()
        except UserData.DoesNotExist:
            print("User does not exist : "+uuid)
        except Exception as e:
            print(str(e))
