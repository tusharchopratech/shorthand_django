import hashlib
import psycopg2
import json
from django.test import TransactionTestCase
from rest_framework.test import RequestsClient


class DictationTest(TransactionTestCase):
    token = ""
    uuid = ""
    email = "tusharchopra12345@gmail.com"
    testDictationUuid = None

    def test_dictation(self):
        try:
            conn = psycopg2.connect("dbname='test_shorthand_dev' user='tushar' host='' password='tushar'")
        except Exception as e:
            assert "Unable to connect database." + str(e)

        print "\n\n****** DICTATION APP TEST ******\n"

        self.checkIfDictationsPresent()
        self.cloningDictationDatabaseForTesting(conn)
        self.checkAddUserApi(conn)
        self.checkGetDictationTypes(conn)
        self.checkGetDictationList(conn)
        self.checkGetImage(conn)
        self.checkGetImageGet()

        conn.close()

    def getSHA(self, uuid, email):
        string = uuid + "volsky" + email
        m = hashlib.sha1()
        m.update(string.encode('utf-8'))
        return m.hexdigest()

    def checkAddUserApi(self, conn):
        url = 'http://testserver/api/v1/users/v1/add_user'
        data = {
            'login_type': 'google',
            'login_unique_id': '1234567890',
            'name': 'tushar',
            'email': self.email
        }
        client = RequestsClient()
        response = client.post(url, data)
        assert response.status_code == 200
        assert response.json()['uuid'] != ''
        assert response.json()['token'] != ''
        assert response.json()['refresh_token'] != ''
        self.token = response.json()['token']
        self.uuid = response.json()['uuid']
        cur = conn.cursor()
        cur.execute("SELECT uuid,name,email,login_type,login_unique_id from user_data;")
        rows = cur.fetchall()
        cur.close()
        assert len(rows) == 1
        assert rows[0][1] == "tushar"
        assert rows[0][2] == self.email
        assert rows[0][3] == "google"
        assert rows[0][4] == "1234567890"
        assert str(rows[0][0]) == self.uuid
        print "Add User API : PASSED"

    def checkIfDictationsPresent(self):
        try:
            conn = psycopg2.connect("dbname='shorthand_dev' user='tushar' host='' password='tushar'")
        except Exception as e:
            assert "Unable to connect database." + str(e)
        cur = conn.cursor()
        cur.execute("SELECT * from dictation;")
        rows = cur.fetchall()
        assert len(rows) >= 144
        cur.execute("SELECT * from dictation_meta;")
        rows = cur.fetchall()
        assert len(rows) >= 144
        cur.close()
        print "Checking Dictation Present In Main DB : PASSED"

    def cloningDictationDatabaseForTesting(self, conn):
        try:
            connMainDb = psycopg2.connect("dbname='shorthand_dev' user='tushar' host='' password='tushar'")
        except Exception as e:
            assert "Unable to connect database." + str(e)

        connMainDb.autocommit = True
        conn.autocommit = True
        cur = connMainDb.cursor()
        cur.execute("SELECT * from dictation;")
        rows = cur.fetchall()
        connMainDb.close()

        cur = conn.cursor()
        cur.execute('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')
        cur.execute('ALTER TABLE dictation ALTER COLUMN uuid SET DEFAULT uuid_generate_v4();')
        cur.execute('ALTER TABLE dictation ALTER COLUMN created SET DEFAULT NOW();')

        for row in rows:
            insertQuery = "insert into dictation(recording_name, speaker_name, speaker_gender, language, topic, speed, duration, audio_file_name, image_file_name) values(%s,%s,%s,%s,%s,%s,%s,%s,%s) returning uuid;"
            cur.execute(insertQuery, (row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10]))
            uuid = cur.fetchone()[0]
            insertQuery = "insert into dictation_meta(recording_name,dictation_uuid, times_played, tags, passage_difficulty, passage_difficulty_total, voice_clarity, voice_clarity_total, modulation, modulation_total) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"
            cur.execute(insertQuery, (row[2], uuid, int(297) * 4, row[6], 0, 0, 0, 0, 0, 0))
        cur.close()
        print "Cloning Dictation Database : PASSED"

    def checkGetDictationTypes(self, conn):
        url = 'http://testserver/api/v1/dictation/v1/get_dictation_types'
        client = RequestsClient()
        headers = {
            "AUTHORIZATION": self.token,
            "KEY": self.getSHA(self.uuid, self.email)
        }
        response = client.post(url, headers=headers)
        assert response.status_code == 200, conn.close()
        assert len(response.json()) == 7, conn.close()
        #print (json.dumps(response.json(), indent=2))
        print "Get Dictation Type API: PASSED"

    def checkGetDictationList(self, conn):
        url = 'http://testserver/api/v1/dictation/v1/get_dictation_list'
        client = RequestsClient()
        headers = {
            "AUTHORIZATION": self.token,
            "KEY": self.getSHA(self.uuid, self.email)
        }
        data = {
            'language': 'english',
            'speed': '80',
            'duration': '10',
        }
        response = client.post(url, data, headers=headers)
        assert response.status_code == 200, conn.close()
        assert len(response.json()) >= 0, conn.close()
        #print (json.dumps(response.json(), indent=2))
        self.testDictationUuid = response.json()[0]['uuid']
        print "Get Dictation List API: PASSED"

    def checkGetImage(self, conn):
        url = 'http://testserver/api/v1/dictation/v1/get_image'
        client = RequestsClient()
        headers = {
            "AUTHORIZATION": self.token,
            "KEY": self.getSHA(self.uuid, self.email)
        }
        data = {
            'dictation_uuid': self.testDictationUuid,
            'image_number': '1',
        }
        response = client.post(url, data, headers=headers)
        assert response.status_code == 200, conn.close()
        print "Get Dictation Image API(POST Request): PASSED"

    def checkGetImageGet(self):
        url = 'http://testserver/api/v1/dictation/v1/get_image'
        client = RequestsClient()
        params = {
            "token": self.token,
            "key": self.getSHA(self.uuid, self.email),
            'dictation_uuid': self.testDictationUuid,
            'image_number': '1',
        }
        response = client.get(url, params=params)
        assert response.status_code == 200
        print "Get Dictation Image API(GET Request): PASSED"