from django.contrib import admin

from .models import Dictation, DictationMeta

admin.site.register(Dictation)
admin.site.register(DictationMeta)
# Register your models here.
