import uuid

from django.db import models


class Dictation(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False, null=False)
    recording_name = models.CharField(max_length=100, null=False)
    speaker_name = models.CharField(max_length=100, null=False)
    speaker_gender = models.CharField(max_length=6, null=False)
    language = models.CharField(max_length=100, null=False)
    topic = models.CharField(max_length=100, null=False)
    times_played = models.IntegerField(default=0)
    tags = models.CharField(max_length=999999)
    speed = models.IntegerField(null=False)
    duration = models.IntegerField(null=False)
    audio_file_name = models.CharField(max_length=1000)
    image_file_name = models.CharField(max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=False)

    class Meta:
       db_table = "dictation"


class DictationMeta(models.Model):
    dictation_uuid = models.UUIDField(unique=True, null=False)
    recording_name = models.CharField(max_length=100, null=False)
    passage_difficulty = models.FloatField(default=0.0, null=False)
    passage_difficulty_total = models.IntegerField(default=0, null=False)
    voice_clarity = models.FloatField(default=0.0, null=False)
    voice_clarity_total = models.IntegerField(default=0, null=False)
    modulation = models.FloatField(default=0.0, null=False)
    modulation_total = models.IntegerField(default=0, null=False)
    created_at = models.DateTimeField(auto_now_add=True, editable=False, null=False)

    class Meta:
        db_table = "dictation_meta"
