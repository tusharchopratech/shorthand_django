from django.apps import AppConfig


class DictationsConfig(AppConfig):
    name = 'apps.dictations'
