import inspect
import os
from wsgiref.util import FileWrapper

from django.http import StreamingHttpResponse
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.general import Auth
from apps.general import Constants
from apps.general import MyLogger


class GetOfflineFile(APIView):
    def get(self, request):

        try:

            token = request.GET.get('token', '')
            key = request.GET.get('key', '')

            if token is None or token == '' or key is None or key == '':
                MyLogger.warning(request, "404", __name__, str(inspect.currentframe().f_lineno))
                return Response(status=status.HTTP_404_NOT_FOUND)
            else:
                obj = Auth.TokenUtils().authenticateUserFromParameters(request, token, key)
                if isinstance(obj, Response):
                    MyLogger.warning(request, "404", __name__, str(inspect.currentframe().f_lineno))
                    return Response(status=status.HTTP_404_NOT_FOUND)
                    # return obj
                else:
                    uuid = obj

                    file = Constants.getOfflineFilePath()
                    wrapper = FileWrapper(open(file, 'rb'))
                    response = StreamingHttpResponse(wrapper, 'application/octet-stream')
                    response['Content-Length'] = os.path.getsize(file)
                    response['Content-Disposition'] = 'attachment; filename=data.data'
                    return response

        except Exception as e:
            MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
