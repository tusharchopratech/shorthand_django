from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.dictations.models import Dictation
from apps.general import Auth
from apps.general import UserLastUsed
from apps.general import MyLogger
import inspect


class GetDictationTypes(APIView):
    def post(self, request):
        try:
            obj = Auth.TokenUtils().authenticateUserFromRequest(request)
            if isinstance(obj, Response):
                return obj
            else:
                uuid = obj
                differentDictationTypes = Dictation.objects.order_by('language', 'speed', 'duration').values('speed',
                                                                                                             'language',
                                                                                                             'duration').distinct()
                UserLastUsed.UserLastUsed().setUserLastUsed(uuid)
                return Response(differentDictationTypes, status=status.HTTP_200_OK)

        except Exception as e:
            MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
