import inspect

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.general import Auth
from apps.general import Constants
from apps.general import MyLogger
from apps.general import UserLastUsed


class GetOfflineFileData(APIView):
    def post(self, request):
        try:
            obj = Auth.TokenUtils().authenticateUserFromRequest(request)
            if isinstance(obj, Response):
                return obj
            else:
                uuid = obj

                offline_file_version = ''
                offline_file_url = ''
                offline_file_password = ''
                offline_file_iv = ''

                offline_file_version = Constants.getOfflineFileVersion()
                offline_file_url = Constants.getOfflineFileUrl()
                offline_file_password = Constants.getOfflineFilePassword()
                offline_file_iv = Constants.getOfflineFileIV()

                response = {
                    'is_payment_done': 'yes',
                    'offline_file_version': offline_file_version,
                    'offline_file_url': offline_file_url,
                    'offline_file_source': 'external',
                    'offline_file_password': offline_file_password,
                    'offline_file_iv': offline_file_iv,
                }
                UserLastUsed.UserLastUsed().setUserLastUsed(uuid)
                return Response(response, status=status.HTTP_200_OK)

        except Exception as e:
            MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
