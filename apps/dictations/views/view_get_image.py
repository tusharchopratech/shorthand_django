import inspect
import os
from wsgiref.util import FileWrapper

import validators
from PIL import Image, ImageDraw, ImageFont, ImageEnhance
from django.http import StreamingHttpResponse
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.dictations.models import Dictation
from apps.general import Auth
from apps.general import Constants
from apps.general import MyLogger


class GetImage(APIView):
    def get(self, request):

        try:
            token = request.GET.get('token', '')
            key = request.GET.get('key', '')
            dictation_uuid = request.GET.get('dictation_uuid', '')
            image_number = request.GET.get('image_number', '')

            if dictation_uuid == '' or token == '' or key == '' or image_number == '' or not validators.uuid(
                    dictation_uuid):
                MyLogger.warning(request, "404", __name__, str(inspect.currentframe().f_lineno))
                return Response(status=status.HTTP_404_NOT_FOUND)
            else:

                obj = Auth.TokenUtils().authenticateUserFromParameters(request, token, key)
                if isinstance(obj, Response):
                    return obj
                else:
                    uuid = obj

                    try:
                        dictation = Dictation.objects.get(uuid=dictation_uuid)
                        recordName = dictation.recording_name
                        imageFileNameList = dictation.image_file_name.split(",")
                        file = Constants.getDictationPath() + recordName + "/" + imageFileNameList[
                            int(image_number) - 1]

                        # tmp_image = tempfile.mktemp()
                        # add_watermark(file, "This is water mark.", out_file=tmp_image, angle=0, opacity=0.95)
                        wrapper = FileWrapper(open(file, 'rb'))
                        response = StreamingHttpResponse(wrapper, 'image/jpeg')
                        response['Content-Length'] = os.path.getsize(file)
                        return response

                    except Dictation.DoesNotExist:
                        MyLogger.warning(request, "404", __name__, str(inspect.currentframe().f_lineno))
                        return Response(status=status.HTTP_404_NOT_FOUND)

        except Exception as e:
            MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


FONT = 'Arial.ttf'


def add_watermark(in_file, text, out_file='watermark.jpg', angle=23, opacity=0.25):
    img = Image.open(in_file).convert('RGB')
    watermark = Image.new('RGBA', img.size, (0, 0, 0, 0))
    size = 2
    n_font = ImageFont.truetype(FONT, size)
    n_width, n_height = n_font.getsize(text)
    while n_width + n_height < watermark.size[0]:
        size += 2
        n_font = ImageFont.truetype(FONT, size)
        n_width, n_height = n_font.getsize(text)
    draw = ImageDraw.Draw(watermark, 'RGBA')
    draw.text(((watermark.size[0] - n_width) / 2,
               (watermark.size[1] - n_height) / 2),
              text, font=n_font)
    watermark = watermark.rotate(angle, Image.BICUBIC)
    alpha = watermark.split()[3]
    alpha = ImageEnhance.Brightness(alpha).enhance(opacity)
    watermark.putalpha(alpha)
    Image.composite(watermark, img, watermark).save(out_file, 'JPEG')
