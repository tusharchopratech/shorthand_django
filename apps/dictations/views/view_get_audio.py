import inspect
import os
from wsgiref.util import FileWrapper

import validators
from django.http import StreamingHttpResponse
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.dictations.models import Dictation
from apps.general import Auth
from apps.general import Constants
from apps.general import MyLogger


class GetAudio(APIView):
    def get(self, request):

        try:
            token = request.GET.get('token', '')
            key = request.GET.get('key', '')
            dictation_uuid = request.GET.get('dictation_uuid', '')

            if dictation_uuid == '' or token == '' or key == '' or not validators.uuid(dictation_uuid):
                MyLogger.warning(request, "404", __name__, str(inspect.currentframe().f_lineno))
                return Response(status=status.HTTP_404_NOT_FOUND)
            else:
                obj = Auth.TokenUtils().authenticateUserFromParameters(request, token, key)
                if isinstance(obj, Response):
                    return obj
                else:
                    uuid = obj

                    try:
                        dictation = Dictation.objects.get(uuid=dictation_uuid)
                        recordName = dictation.recording_name
                        audioFileName = dictation.audio_file_name
                        file_mp3 = Constants.getDictationPath() + recordName + "/" + audioFileName

                        wrapper = FileWrapper(open(file_mp3, 'rb'))
                        response = StreamingHttpResponse(wrapper, 'audio/mp3')
                        response['Content-Length'] = os.path.getsize(file_mp3)
                        return response

                    except Dictation.DoesNotExist:
                        MyLogger.warning(request, "404", __name__, str(inspect.currentframe().f_lineno))
                        return Response(status=status.HTTP_404_NOT_FOUND)

        except Exception as e:
            MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
