import inspect

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.dictations.models import Dictation, DictationMeta
from apps.general import Auth
from apps.general import MyLogger
from apps.general import UserLastUsed
from apps.users.models import UserData


class GetDictationList(APIView):
    def post(self, request):
        try:
            obj = Auth.TokenUtils().authenticateUserFromRequest(request)
            if isinstance(obj, Response):
                return obj
            else:
                uuid = obj

                data = request.data
                language = data.get('language')
                speed = data.get('speed')
                duration = data.get('duration')
                if language is None or language == "" or speed is None or speed == "" or duration is None or duration == "":
                    MyLogger.warning(request, "400", __name__, str(inspect.currentframe().f_lineno))
                    return Response(status=status.HTTP_400_BAD_REQUEST)
                else:
                    dictationList = Dictation.objects.filter(language=language, speed=speed,
                                                             duration=duration).order_by('-times_played')
                    userData = None
                    try:
                        userData = UserData.objects.get(uuid=uuid)
                    except Exception as e:
                        MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
                        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

                    response = []

                    for dictation in dictationList:
                        dictationMeta = DictationMeta.objects.get(dictation_uuid=dictation.uuid)

                        practiced_status = "new"
                        if userData is not None and userData.practiced is not None and str(
                                dictation.uuid) in userData.practiced:
                            practiced_status = "done"
                        elif userData is not None and userData.viewed is not None and str(
                                dictation.uuid) in userData.viewed:
                            practiced_status = "viewed"

                        meta = {
                            'passage_difficulty': dictationMeta.passage_difficulty,
                            'passage_difficulty_total': dictationMeta.passage_difficulty_total,
                            'voice_clarity': dictationMeta.voice_clarity,
                            'voice_clarity_total': dictationMeta.voice_clarity_total,
                            'modulation': dictationMeta.modulation,
                            'modulation_total': dictationMeta.modulation_total,
                            'times_played': dictation.times_played,
                            'tags': dictation.tags,
                            'practiced_status': practiced_status
                        }

                        item = {
                            'uuid': dictation.uuid,
                            'topic': dictation.topic,
                            'speaker_name': dictation.speaker_name,
                            'speaker_gender': dictation.speaker_gender,
                            'speed': dictation.speed,
                            'language': dictation.language,
                            'duration': dictation.duration,
                            'images': len(dictation.image_file_name.split(",")),
                            'meta': meta
                        }
                        response.append(item)

                    if len(response) == 0:
                        MyLogger.warning(request, "406", __name__, str(inspect.currentframe().f_lineno))
                        return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
                    else:
                        UserLastUsed.UserLastUsed().setUserLastUsed(uuid)
                        return Response(response, status=status.HTTP_200_OK)

        except Exception as e:
            MyLogger.error(request, "500", __name__, str(inspect.currentframe().f_lineno), str(e))
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
