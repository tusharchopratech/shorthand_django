from django.shortcuts import render

def index(request):
    context = {'title': 'shorthandpractice.com'}
    return render(request, 'home/index2.html', context)
